"""Use yampy to query the Yammer API and return message data.

Likes (per message ID)
Shares (per message ID)
Comments (per message ID)

Get all message IDs from 1st January to date.
Use the /messages/in_thread to return figures per Message ID.
"""

import yampy
import pandas as pd
import time
import os
from urllib2 import urlparse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# import os
import glob
import zipfile


def get_access_token(id_path, secret_path):
    """Get OAuth details and authenticate with yammer."""
    with open(id_path, 'r') as f:
        client_id = f.read()

    with open(secret_path, 'r') as f:
        client_secret = f.read()

    # Create an authenticator object to get an access token
    authenticator = yampy.Authenticator(client_id, client_secret)

    # This is the URL that is set in the app on Yammer.
    # When we authenticate, we'll be redirected to this url.
    redirect_uri = "https://www.standardbank.com/"

    # Build the authorisation URL
    auth_url = authenticator.authorization_url(redirect_uri=redirect_uri)

    # Need to use Selenium to make the web call in a browser
    # This is because it needs user/password
    # Then a wait/redirect happens before the code parameter appears.
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    # Start a chrome connection and open the auth_url
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get(auth_url)

    # Find login elements and supply keys (yammer)
    login = driver.find_element_by_name('login')
    password = driver.find_element_by_name('password')

    login.send_keys('Colleen.rose@standardbank.co.za')
    password.send_keys('Elenov8!')

    # Wait for the next authentication screen (microsoft)
    time.sleep(10)

    login = driver.find_element_by_name('UserName')
    password = driver.find_element_by_name('Password')

    login.send_keys('Colleen.rose@standardbank.co.za')
    password.send_keys('Elenov8!')

    driver.find_element_by_id("submitButton").click()

    # Wait for the last redirection to happen
    time.sleep(5)
    driver.find_element_by_id("idSIButton9").click()
    time.sleep(5)

    # Now the "code" parameter is ready to be parsed from the URL
    url = driver.current_url
    driver.close()
    parsed = urlparse.urlparse(url)
    code = urlparse.parse_qs(parsed.query)['code'][0]
    access_token = authenticator.fetch_access_token(code)

    return(access_token)


def export_data(access_token):
    """Download Zip file using the data export API."""
    url = ('https://export.yammer.com/api/v1/export'
           '?since=2018-01-01T00:00:00+00:00&model=Message'
           '&include=csv&access_token=') + access_token

    driver = webdriver.Chrome()

    driver.get(url)

    time.sleep(300)

    driver.close()


def clear_previous_exports():
    """Remove previous exports fromt the downloads folder and working dir."""
    folder = r'C:\Users\mburrows\Downloads'
    for filename in os.listdir(folder):
        string_list = ['export-', '.zip']
        if all(item in filename for item in string_list):
            os.remove(folder + '\\' + filename)

    folder = os.getcwd()
    for filename in os.listdir(folder):
        string_list = ['export-', '.zip']
        if all(item in filename for item in string_list):
            os.remove(folder + '\\' + filename)


def move_to_working_location():
    """Move from downloads folder to the working directory."""
    source_directory = r'C:\Users\mburrows\Downloads'
    target_directory = os.getcwd()
    for filename in os.listdir(source_directory):
        string_list = ['export-', '.zip']
        if all(item in filename for item in string_list):
            source_file = source_directory + '\\' + filename
            target_file = target_directory + '\\' + filename
            os.rename(source_file, target_file)


def get_messages_from_zip():
    """Create a dataframe of message IDs.

    Open a file called Messages.csv from a Zip file in working directory.
    Read it into a dataframe.
    Filter it to non-private messages only.
    Return a dataframe of Message IDs.
    """
    # Get the name of the zip file
    filename = glob.glob('./*.zip')[0].replace('\\', '').strip('.')

    # Read the Messages.csv file into a dataframe
    zf = zipfile.ZipFile(filename)
    df = pd.read_csv(zf.open('Messages.csv'))

    messages = df[[
        'id',
        'replied_to_id',
        'thread_id',
        'created_at',
        'in_private_group',
        'in_private_conversation',
        'body',
        'deleted_at']]

    return messages


def add_comments_column(df):
    """Return the number of comments each message received.

    Reads the messages csv in the zip file downloaded by export_data.
    pandas.to_csv method can be used on the results to output to a csv.
    """
    # filename = glob.glob('./*.zip')[0].replace('\\', '').strip('.')

    # # Read the Messages.csv file into a dataframe
    # zf = zipfile.ZipFile(filename)
    # df = pd.read_csv(zf.open('Messages.csv'))

    id_list = df[
        (df['thread_id'] != df['id']) &  # Don't count original message
        (df['in_private_group'] == False) &  # Only public comments
        (df['in_private_conversation'] == False)][[  # Only public comments
            'id',
            'thread_id']]

    comments = id_list.groupby(['thread_id']).agg(['count']).drop_duplicates()

    comments.columns = comments.columns.droplevel(0)

    comments = comments.rename(columns={'count': 'comments'})

    df = pd.merge(
        df,
        comments,
        left_on='id',
        right_on='thread_id',
        how='left')

    # Dedupe. Because there are multiple messages against a thread_id,
    # ... ensure that you only store the number of comments against
    # ... the first message in each thread.
    for index, row in df.iterrows():
        if row['id'] != row['thread_id']:
            row['comments'] = 0

    df['comments'] = df['comments'].fillna(value=0)

    return df


def add_mentions(df):
    """Loop through the messages and count how many users are mentioned."""
    mentions_list = []

    for row in df[['id', 'body']].iterrows():
        mentions = {
            'id': row[1][0],
            'mentions': str(row[1][1]).count('[user:')}

        mentions_list.append(mentions)

    mentions = pd.DataFrame(mentions_list).drop_duplicates()

    df = pd.merge(
        df,
        mentions,
        left_on='id',
        right_on='id',
        how='left')

    return df


def get_thread_stats(messages):
    """Loop through each thread_id and find likes/shares/topics.

    Returns a dictionary of DataFrames.
    """
    # Create a deduplicated list of thread IDs
    threads = list(set(messages['thread_id']))

    print(len(threads))

    all_shares = []
    all_likes = []
    all_topics = []

    for thread_id in threads:
        try:
            time.sleep(3)
            # Add a try/except
            data = yammer.messages.in_thread(int(thread_id))

            try:
                for reference in data['references']:
                    if 'stats' in reference:
                        if 'shares' in reference['stats']:
                            shares = reference['stats']['shares']

                thread_shares = {
                    'thread_id': thread_id,
                    'shares': shares
                }

                all_shares.append(thread_shares)
            except:
                pass

            try:
                for message in data['messages']:
                    message_id = message['id']
                    likes = message['liked_by']['count']
                    # if 'liked_by' in bit:
                    #     likes += int(bit['liked_by']['count'])

                    message_likes = {
                        'id': message_id,
                        'likes': likes
                    }

                    all_likes.append(message_likes)
            except:
                pass

            try:
                topic_references = data['references']
                for reference in topic_references:
                    if 'normalized_name' in reference:
                        thread_topic = {
                            'thread_id': thread_id,
                            'topic': reference['normalized_name']}
                        all_topics.append(thread_topic)
                time.sleep(3)
            except:
                pass
        except:
            pass

    shares = pd.DataFrame(all_shares)
    likes = pd.DataFrame(all_likes)
    topics = pd.DataFrame(all_topics)

    df_dict = {
        'shares': shares,
        'likes': likes,
        'topics': topics
    }

    return df_dict


if __name__ == '__main__':

    clear_previous_exports()

    client_id = str('C:\Users\mburrows\Documents\Projects'
                    '\Standard Bank Visualisation\OAuth\client_id.txt')

    client_secret = str('C:\Users\mburrows\Documents\Projects'
                        '\Standard Bank Visualisation\OAuth\client_secret.txt')

    # # First get an access token to use the API:
    print('Authenticating with Yammer')
    access_token = get_access_token(client_id, client_secret)
    print(access_token)
    # # Export the data in a zip file
    export_data(access_token)

    # # Move the export from downloads to the working directory
    move_to_working_location()

    # # Read all the messages from the zip file into a pandas dataframe
    print('Extracting messages from zip')
    all_messages = get_messages_from_zip()

    # Start a yammer api instance
    yammer = yampy.Yammer(access_token)

    # Filter the messages to non-private only (no likes or shares on PMs)
    print('Filtering messages')
    messages = all_messages[
        (all_messages['in_private_group'] == False) &  # Only public comments
        (all_messages['in_private_conversation'] == False)]

    # Add a column showing how many comments are on each thread
    print('Fetching comments')
    messages = add_comments_column(messages)

    # Add a column showing how many mentions are on each thread
    print('Fetching mentions')
    messages = add_mentions(messages)

    # Create a dictionary of frames for likes, shares and topics
    print('fetching stats')
    stats = get_thread_stats(messages)

    # Merge the columns onto the messages dataframe to form the report
    try:
        report = pd.merge(
            messages,
            stats['likes'],
            left_on='id',
            right_on='id',
            how='left')

    except IndexError:
        report = messages.reindex_axis(
            messages.columns.union(
                stats['likes'].columns), axis=1)

    try:
        report = pd.merge(
            report,
            stats['shares'],
            left_on='id',
            right_on='thread_id',
            how='left')

    except IndexError:
        report = report.reindex_axis(
            report.columns.union(
                stats['shares'].columns), axis=1)

    report = report[[
        'id',
        'replied_to_id',
        'thread_id_x',
        'created_at',
        'in_private_group',
        'in_private_conversation',
        'body',
        'deleted_at',
        'comments',
        'mentions',
        'likes',
        'shares']]

    report.rename(columns={'thread_id_x': 'thread_id'}, inplace=True)

    report.fillna(0, inplace=True)

    print(report)

    # Format the report columns and output to CSV
    print('Writing to CSV')
    report.to_csv('report.csv', index=False)
    stats['topics'].to_csv('topics.csv', index=False)
